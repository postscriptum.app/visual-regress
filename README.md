# visual-regress

A graphical and command line utility for launching and visualizing regression tests.

## License

[MIT](https://gitlab.com/postscriptum.app/core/-/blob/master/LICENSE)
