import {ipcRenderer, shell} from 'electron';
import type {Input, InputsTree} from "./scanner";

const ITEM_STATES = ['error', 'approved', 'rejected', 'conflict', 'tested'];

class InputItem extends HTMLElement {
	protected _radio: HTMLInputElement;
	input: Input;

	get selected(): boolean {
		return this.hasAttribute('selected');
	}

	set selected(val: boolean) {
		if (val) this.setAttribute('selected', '');
		else this.removeAttribute('selected');
	}

	initialize(input: Input): this {
		this.input = input;
		this.attachShadow({mode: 'open'});
		const style = this.shadowRoot.appendChild(document.createElement('style'));
		/* language=CSS */
		style.textContent = `
			:host {
				display: block;
				padding: 0.1em 0.4em;
				cursor: pointer;
			}

			:host([selected]) {
				background-color: #b5d2fb;
				border-color: lightgrey;
			}
		`;

		const span = this.shadowRoot.appendChild(document.createElement('span'));
		span.textContent = input.name;

		return this;
	}
}

customElements.define('vr-input-item', InputItem);


class InputsFolder extends HTMLElement {
	protected _checkbox: HTMLInputElement;

	get opened(): boolean {
		return this._checkbox.checked;
	}

	set opened(val: boolean) {
		this._checkbox.checked = val;
		this._onChange();
	}

	initialize(name: string): this {
		this.attachShadow({mode: 'open'});
		const style = this.shadowRoot.appendChild(document.createElement('style'));
		/* language=CSS */
		style.textContent = `
			:host {
				display: block;
			}

			input {
				display: none;
			}

			label {
				display: flex;
			}

			label::before {
				content: '\\f07b\\A0';
				display: flex;
				font-family: 'FontAwesome';
				align-items: center;
				justify-content: center;
			}

			:host([opened]) label::before {
				content: '\\f07c\\A0';
			}

			div {
				display: none;
				margin-left: 0.3em;
				padding-left: 1em;
				list-style-type: none;
				border-left: solid 1px #f0f0f0;
			}

			:host([opened]) div {
				display: block;
			}
		`;
		const label = this.shadowRoot.appendChild(document.createElement('label'));
		const checkbox = this._checkbox = label.appendChild(document.createElement('input'));
		checkbox.type = 'checkbox';
		checkbox.checked = true;
		const labelSpan = label.appendChild(document.createElement('span'));
		labelSpan.textContent = name;

		const childrenList = this.shadowRoot.appendChild(document.createElement('div'));
		childrenList.appendChild(document.createElement('slot'));

		checkbox.onchange = () => this._onChange();

		return this;
	}

	protected _onChange(): void {
		if (this._checkbox.checked) this.setAttribute('opened', '');
		else this.removeAttribute('opened');
	}
}

customElements.define('vr-inputs-folder', InputsFolder);

const $: {
	splitView: HTMLInputElement,
	sourceView: HTMLInputElement,
	noteText: HTMLTextAreaElement,
	note: HTMLInputElement,
	editNote: HTMLInputElement,
	tree: HTMLFormElement,
	next: HTMLButtonElement,
	previous: HTMLButtonElement,
	approve: HTMLButtonElement,
	reject: HTMLButtonElement,
	tools: HTMLInputElement,
	baseFrame: HTMLIFrameElement,
	mainFrame: HTMLIFrameElement,
	[key: string]: HTMLElement
} & { filterInputs: NodeListOf<HTMLInputElement> } = {} as any;

let currentInput: Input;
const inputItems: { [path: string]: InputItem } = {};
let previousItem, nextItem, visibleItems;

document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('[id]').forEach((element) => {
		$[element.id] = element as HTMLElement;
	});

	$.filterInputs = $.filters.querySelectorAll<HTMLInputElement>('input.filter');

	visibleItems = document.createTreeWalker($.tree, NodeFilter.SHOW_ELEMENT, {
		acceptNode: (node): number => {
			if (node instanceof InputItem) return node.hidden ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT;
			return NodeFilter.FILTER_SKIP;
		}
	});
	// Views
	$.externalView.onclick = () => {
		shell.openPath(currentInput.path);
	};

	$.sourceView.onclick = () => {
		$.splitView.checked = false;
		updateCurrent();
	};

	$.splitView.onclick = () => {
		$.sourceView.checked = false;
		updateCurrent();
	};

	// Operation
	$.test.onclick = testCurrent;

	$.approve.onclick = () => {
		ipcRenderer.send('commit', currentInput, true);
		updateFilters();
		next();
	};
	$.reject.onclick = () => {
		ipcRenderer.send('commit', currentInput, false);
		updateFilters();
		next();
	};

	$.testFiltered.onclick = () => {
		const inputs = Array.prototype.map.call($.tree.querySelectorAll('vr-input-item:not([hidden])'), (item) => item.input);
		if (inputs.length) ipcRenderer.send('test', inputs);
	};

	// Navigation
	$.next.onclick = next;
	$.previous.onclick = previous;

	// Note
	$.note.onclick = () => {
		toggleNote($.note.checked);
		if ($.note.checked && !currentInput.noteFile) {
			editNote();
		}
	};
	$.editNote.onclick = () => {
		if ($.editNote.checked) editNote();
		else saveNote();
	};
	$.deleteNote.onclick = () => {
		$.noteText.value = '';
		saveNote();
	};
	// Filters
	for (const filterInput of $.filterInputs) {
		filterInput.onclick = function (this: HTMLInputElement): void {
			updateFilters(this);
		};
	}
	//$.conflictFilter.onclick = $.approvedFilter.onclick = $.filterRejected.onclick = $.filterError.onclick = updateFilters;
});

ipcRenderer.on('inputs', (event, inputs) => {
	while ($.tree.lastChild) $.tree.lastChild.remove();
	inputsList(inputs, $.tree);
	const firstItem = $.tree.querySelector('vr-input-item') as InputItem;
	if (firstItem) setCurrent(firstItem.input);
	updateFilters();
	updateNav();
});


function inputsList(inputsTree: InputsTree, parent: HTMLElement): void {
	for (const name in inputsTree) {
		const object = inputsTree[name];
		if ('__input' in object) {
			const item = parent.appendChild(new InputItem().initialize(object as Input));
			item.onclick = () => {
				setCurrent(item.input);
				updateNav();
			};
			inputItems[(object as Input).path] = item;
			updateItem(object as Input);
		} else {
			const folder = parent.appendChild(new InputsFolder().initialize(name));
			folder.opened = true;
			inputsList(object, folder);
		}
	}
}

ipcRenderer.on('inputChanged', (event, input: Input) => {
	// TODO working directly with the input of the main process can crash the dev-tools
	const localInput = Object.assign({}, input) as Input;
	updateItem(localInput);
	if (localInput.path == currentInput.path) {
		currentInput = localInput;
		updateCurrent();
	}
	updateFilters();
});

function updateItem(input: Input): void {
	const item = inputItems[input.path];
	item.input = input;
	for (const state of ITEM_STATES) item.classList.remove(state);
	if (input.state) item.classList.add(input.state);
	if (input.processing) item.classList.add('processing');
	else item.classList.remove('processing');
	if (input.noteFile) item.classList.add('note');
	else item.classList.remove('note');
}

async function setCurrent(input: Input): Promise<void> {
	if (currentInput == input) return;

	for (const path in inputItems) {
		const item = inputItems[path];
		if (path == input.path) {
			item.selected = true;
			let parent = item.parentElement;
			while (parent instanceof InputsFolder) {
				parent.opened = true;
				parent = parent.parentElement;
			}
		} else {
			item.selected = false;
		}
	}
	currentInput = input;

	$.filename.textContent = input.relPath;

	$.assert.textContent = '';
	if (input.extname.match(/^\.(html|xhtml|htm|xht)$/)) {
		const resp = await load(input.path);
		const assert = resp.querySelector('html > head > meta[name=assert]');
		if (assert) $.assert.textContent = assert.getAttribute('content');
	}

	$.noteText.value = input.noteFile ? input.note : '';
	toggleNote('noteFile' in input);

	await updateCurrent();
}

let unregisterSync: () => void;

async function updateCurrent(): Promise<void> {
	ITEM_STATES.forEach((state) => $.filename.classList.remove(state));

	if (currentInput.state) $.filename.classList.add(currentInput.state);
	if (currentInput.processing) $.filename.classList.add('processing');
	else $.filename.classList.remove('processing');
	if (currentInput.noteFile) $.filename.classList.add('note');
	else $.filename.classList.remove('note');

	$.filename.classList.remove('baseApproved', 'baseRejected');
	if (currentInput.state == 'conflict') {
		$.filename.classList.add(currentInput.approvedFile ? 'baseApproved' : 'baseRejected');
	}

	const commitable = !currentInput.state || currentInput.state == 'error';
	$.approve.disabled = commitable || currentInput.state == 'approved';
	$.reject.disabled = commitable || currentInput.state == 'rejected';

	$.splitView.hidden = currentInput.state != 'conflict';

	$.main.className = currentInput.processing ? 'processing' : '';

	if (!currentInput.processing) {
		if (unregisterSync) {
			unregisterSync();
			unregisterSync = null;
		}
		if (currentInput.state == 'conflict' && $.splitView.checked) {
			$.baseFrame.hidden = false;
			unregisterSync = await synchronizeViews(currentInput.testFile, currentInput.approvedFile || currentInput.rejectedFile);
		} else {
			$.baseFrame.hidden = true;
			if ($.sourceView.checked) view(currentInput.path);
			else if (currentInput.state == 'error') view(currentInput.errorFile);
			else if (currentInput.state == 'approved') view(currentInput.approvedFile);
			else if (currentInput.state == 'rejected') view(currentInput.rejectedFile);
			else if (currentInput.state == 'conflict') view(currentInput.diffFile);
			else if (currentInput.state == 'tested') view(currentInput.testFile);
			else view(new URL('no-test.html', window.location.href));
		}
	}
}

function updateFilters(currentFilter?: HTMLInputElement): void {

	if (currentFilter) {
		for (const filterInput of $.filterInputs) {
			if (filterInput != currentFilter) filterInput.checked = false;
		}
	} else {
		currentFilter = $.filters.querySelector('input.filter:checked');
	}

	let processingCount = 0;
	const filtersCount: { [ state: string] : number } = { };
	for (const item of $.tree.querySelectorAll<InputItem>('vr-input-item')) {
		item.hidden = currentFilter && item.input.state != currentFilter.value && currentFilter.checked;

		if (!(item.input.state in filtersCount)) filtersCount[item.input.state] = 0;
		filtersCount[item.input.state]++;

		if (item.classList.contains('processing')) processingCount++;
	}

	for (const folder of $.tree.querySelectorAll<InputsFolder>('vr-inputs-folder')) {
		const visibleItems = folder.querySelectorAll('vr-input-item:not([hidden])');
		folder.hidden = !visibleItems.length;
	}

	$.processingCount.textContent = processingCount ? processingCount.toString() : '';

	for (const filterInput of $.filterInputs) {
		filterInput.nextElementSibling.textContent = filterInput.value in filtersCount ? filtersCount[filterInput.value].toString() : '0';
	}

	updateNav();
}

function updateNav(): void {
	const item = inputItems[currentInput.path];

	visibleItems.currentNode = item;
	nextItem = visibleItems.nextNode();
	$.next.disabled = !nextItem;
	visibleItems.currentNode = item;
	previousItem = visibleItems.previousNode();
	$.previous.disabled = !previousItem;
}


ipcRenderer.on('tools', (event, tools) => {
	if (tools.viewers) for (const key in tools.viewers) {
		const toolBtn = $.toolsMenu.appendChild(document.createElement('button'));
		toolBtn.textContent = tools.viewers[key];
		toolBtn.onclick = () => {
			ipcRenderer.send('tool', 'viewer', key, currentInput);
			$.tools.checked = false;
		};
	}
	if (tools.processors) for (const key in tools.processors) {
		const toolBtn = $.toolsMenu.appendChild(document.createElement('button'));
		toolBtn.textContent = tools.processors[key];
		toolBtn.onclick = () => {
			ipcRenderer.send('tool', 'processor', key, currentInput);
			$.tools.checked = false;
		};
	}
});

function load(url: string): Promise<XMLDocument> {
	return new Promise((resolve, reject) => {
		const request = new XMLHttpRequest();
		request.open('GET', url);
		request.responseType = "document";
		request.onload = () => resolve(request.responseXML);
		request.onerror = () => reject(new Error("Unable to fetch '" + url + "'"));
		request.send();
	});
}


function view(file: string | URL, frame: HTMLIFrameElement = $.mainFrame): Promise<Window> {
	const url = file instanceof URL ? file.href : 'file://' + file;
	return new Promise<Window>((resolve, reject) => {
		frame.src = 'about:blank';
		frame.addEventListener('load', () => {
			frame.addEventListener('error', reject, { once: true });
			if (url.match(/\.pdf$/)) {
				const viewerUrl = new URL('./pdf.js.asar/web/viewer.html', window.location.href);
				viewerUrl.search = '?file=' + encodeURI(url);
				viewerUrl.hash = '#zoom=page-fit&disablehistory=true';
				frame.addEventListener('load', () => {
					frame.contentWindow.PDFViewerApplication.initializedPromise.then(() => {
						frame.contentWindow.PDFViewerApplication.eventBus.on('pagesinit', () => resolve(frame.contentWindow), { once: true });
					});
				}, { once: true });
				frame.src = viewerUrl.href;
			} else {
				frame.addEventListener('load', () => resolve(frame.contentWindow));
				frame.src = url;
			}
		}, {once: true});
	});
}

async function synchronizeViews(mainFile: string, baseFile: string): Promise<() => void> {
	const viewMain = view(mainFile);
	const viewBase = view(baseFile, $.baseFrame);
	const [mainWin, baseWin] = await Promise.all([viewMain, viewBase]);
	// Synchronize views scroll
	let eventFrom = null;
	let unregister;

	const mainViewer = mainWin.PDFViewerApplication;
	const baseViewer = baseWin.PDFViewerApplication;

	if (mainViewer && baseViewer) {
		const mainListener = (event): void => {
			eventFrom = 'main';
			const scale = event.location.scale;
			baseViewer.pdfViewer.currentScaleValue = typeof scale == 'number' ? scale / 100 : scale;
			baseViewer.pdfViewer.scrollPageIntoView({
				pageNumber: event.location.pageNumber,
				destArray: [null, {name: 'XYZ',}, event.location.left, event.location.top, null],
				allowNegativeOffset: true,
			});
			setTimeout(() => {
				eventFrom = null;
				//baseViewer.eventBus.on('updateviewarea', baseListener);
			}, 1000);
		};

		mainViewer.eventBus.on('updateviewarea', mainListener);

		unregister = () => {
			mainViewer.eventBus.off('updateviewarea', mainListener);
		};
	} else {
		const mainListener = (event: UIEvent): void => {
			if (eventFrom == 'base') return;
			eventFrom = 'main';
			if (event.target == mainWin.document) {
				baseWin.scrollTo(mainWin.scrollX, mainWin.scrollY);
			} else if (event.target instanceof Element && event.target.id) {
				const baseTarget = baseWin.document.getElementById(event.target.id);
				baseTarget.scrollTo(event.target.scrollLeft, event.target.scrollTop);
			}
			eventFrom = null;
		};


		const baseListener = (event: UIEvent): void => {
			if (eventFrom == 'main') return;
			eventFrom = 'base';
			if (event.target == baseWin.document) {
				mainWin.scrollTo(baseWin.scrollX, baseWin.scrollY);
			} else if (event.target instanceof Element && event.target.id) {
				const mainTarget = mainWin.document.getElementById(event.target.id);
				mainTarget.scrollTo(event.target.scrollLeft, event.target.scrollTop);
			}
			eventFrom = null;
		};

		mainWin.addEventListener('scroll', mainListener, true);
		baseWin.addEventListener('scroll', baseListener, true);
		unregister = () => {
			mainWin.removeEventListener('scroll', mainListener, true);
			baseWin.removeEventListener('scroll', mainListener, true);
		};
	}
	return unregister;
}

function toggleNote(show: boolean): void {
	$.note.checked = show;
	$.notePanel.hidden = !show;
	$.editNote.checked = false;
	$.noteText.disabled = true;
}

function editNote(): void {
	$.editNote.checked = true;
	$.noteText.disabled = false;
	$.noteText.focus();
}

function saveNote(): void {
	ipcRenderer.send('note', currentInput, $.noteText.value);
	$.noteText.disabled = true;
	if (!$.noteText.value) toggleNote(false);
}

function next(): void {
	if (nextItem) {
		setCurrent(nextItem.input);
		updateNav();
	}
}

function previous(): void {
	if (previousItem) {
		setCurrent(previousItem.input);
		updateNav();
	}
}

export function testCurrent(): void {
	ipcRenderer.send('test', currentInput);
}
