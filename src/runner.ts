import * as fs from "fs";
import * as path from "path";
import * as childProcess from "child_process";

import * as util from "./util.js";
import type {Command, ComplexCommmand, Props} from "./config.js";
import type {Input} from "./scanner.js";

const fsp = fs.promises;

class RunError extends Error {
	constructor(message: string, readonly exitCode: number, readonly signal: string) {
		super(message);
	}
}

async function test(input: Input): Promise<boolean> {
	const testFile = util.pathReplace(global.config.outputs.test, input as unknown as Props);

	await fsp.rm(testFile, {force: true});
	await fsp.mkdir(path.dirname(testFile), {recursive: true});
	const errorFile = util.pathReplace(global.config.outputs.error, input as unknown as Props);
	await run(global.config.processor, {input: input.path, testOutput: testFile}, errorFile);
	if (input.state == 'approved' || input.state == 'rejected' || input.state == 'conflict') {
		return await compare(input, testFile);
	}
	return false;
}

async function compare(input: Input, testFile: string): Promise<boolean> {
	const baseFile = input.approvedFile || input.rejectedFile;
	const diffFile = util.pathReplace(global.config.outputs.diff, input as unknown as Props);
	const errorFile = util.pathReplace(global.config.outputs.error, input as unknown as Props);

	await Promise.all([diffFile, errorFile].map((file) => fsp.mkdir(path.dirname(file), {recursive: true})));

	await fsp.rm(diffFile, {force: true});
	try {
		await run(global.config.comparator, {fileA: baseFile, fileB: testFile, diffOutput: diffFile}, errorFile);
	} catch (error) {
		if (error instanceof RunError) {
			if (error.exitCode == 1) {
				await fsp.rm(errorFile, {force: true});
				return true;
			} else throw error;
		}
	}

	await Promise.all([testFile, diffFile].map((file) => fsp.rm(file, {force: true})));

	return false;
}

async function run(command: Command, props: Props, errorFile: string): Promise<void> {
	if (typeof command == 'function') {
		const result = command(props);
		if (result instanceof Promise) command = await result;
		else command = result;
	}

	if (typeof command == 'string') {
		const argv = command.split(' ');
		command = {
			executable: argv[0],
			args: argv.slice(1).filter((arg) => arg.length),
		};
	}
	command.executable = util.osPath(command.executable);
	const args = command.args.map((arg) => util.pathReplace(arg, props));
	const env = command.env ? Object.assign({}, process.env, command.env) : process.env;

	await fsp.rm(errorFile, {force: true});

	const child = childProcess.spawn(command.executable, args, {cwd: command.cwd || global.config.baseDir, env});
	let output = "";
	if (process.platform == 'win32') child.stdout.on('data', (data) => output += data);
	child.stderr.on('data', (data) => output += data);

	let killedByTimeout = false;
	const killTimeout = setTimeout(() => {
		child.kill('SIGKILL');
		killedByTimeout = true;
	}, command.timeout || 5000);

	return new Promise<void>((resolve, reject) => {
		child.on('close', (code, signal) => {
			clearTimeout(killTimeout);
			if (code || signal) {
				let errorHeader = 'Command: ' + (command as ComplexCommmand).executable + ' ' + args.join(' ') + '\r\n';
				if (code) errorHeader += 'Exit code: ' + code + '\r\n';
				if (killedByTimeout) errorHeader += 'Killed by timeout\r\n';
				errorHeader += '\r\n';
				fs.mkdirSync(path.dirname(errorFile), {recursive: true});
				fs.writeFile(errorFile, errorHeader + '\r\n' + output, (writeError) => {
					if (writeError) reject(writeError);
					else {
						const error = new RunError(errorHeader, code, signal);
						reject(error);
					}
				});
			} else {
				resolve();
			}
		});
	});
}


async function launch(command: Command, props: Props): Promise<void> {
	if (typeof command == 'function') {
		const result = command(props);
		if (result instanceof Promise) command = await result;
		else command = result;
	}

	if (typeof command == 'string') {
		const argv = command.split(' ');
		command = {
			executable: argv[0],
			args: argv.slice(1),
		};
	}

	command.executable = util.osPath(command.executable);
	const args = command.args.map((arg) => util.pathReplace(arg, props));
	childProcess.spawn(command.executable, args, {cwd: command.cwd || global.config.baseDir, env: command.env || process.env});
}

export {test, compare, run, launch};

