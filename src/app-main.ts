import * as fs from "fs";
import * as path from "path";
import * as os from "os";

import {BrowserWindow, ipcMain} from "electron";

import * as commander from "commander";
import * as util from "./util.js";
import * as scanner from "./scanner.js";
import * as runner from "./runner.js";
import {createConfig} from "./config.js";
import type {Props} from "./config.js";
import type {Input} from "./scanner.js";
import {asyncPool} from "./util.js";

const {app} = require("electron");
const fsp = fs.promises;

app.commandLine.appendSwitch('enable-experimental-web-platform-features');

app.on('window-all-closed', () => {
	app.quit();
});

const command = new commander.Command()
	.argument("<configFile>")
	.argument("[baseDir]")
	.showHelpAfterError()
	.parse(process.argv.filter((arg) => !arg.startsWith('--')));
const [configFile, baseDir] = command.processedArgs;
const config = global.config = createConfig(configFile, baseDir);
let mainWindow;
(async () => {
	try {
		mainWindow = await createMainWindow();
		mainWindow.webContents.on('dom-ready', async () => {
			const inputsList = await scanner.scan();
			await mainWindow.webContents.send('inputs', scanner.inputsTree(inputsList));
			if (config.tools) mainWindow.webContents.send('tools', {
				processors: Object.keys(config.tools.processors).reduce((result, key) => {
					result[key] = config.tools.processors[key].label;
					return result;
				}, {}),
				viewers: Object.keys(config.tools.viewers).reduce((result, key) => {
					result[key] = config.tools.viewers[key].label;
					return result;
				}, {}),
			});
		});
	} catch (error) {
		console.error(error.stack || error.message || error);
	}
})();

function createMainWindow(): Promise<BrowserWindow> {
	return new Promise<BrowserWindow>((resolve) => {
		app.once('ready', () => {
			const window = new BrowserWindow({
				width: 1000,
				height: 800,
				webPreferences: {
					webSecurity: true,
					plugins: true,
					nodeIntegration: true,
					contextIsolation: false,
					devTools: true
				},
			});
			window.loadURL(`file://${app.getAppPath()}/web/index.html`);
			resolve(window);
		});
	});
}

ipcMain.on('scan', async () => {
	try {
		const inputsTree = await scanner.scan();
		mainWindow.webContents.send('inputs', inputsTree);
	} catch (error) {
		console.error(error.stack || error.message || error);
	}
});

async function testInput(input: Input): Promise<Input> {
	try {
		await runner.test(input);
	} catch (error) {
		console.error(error.stack || error.message || error);
	}
	return input;
}

ipcMain.on('test', async (event, inputs: Input | Input[]) => {
	if (!Array.isArray(inputs)) inputs = [inputs];

	for (const input of inputs) {
		input.processing = true;
		mainWindow.webContents.send('inputChanged', input);
	}

	const concurrency = config.concurrency || Math.floor(os.cpus().length / 2);
	for await (const input of asyncPool(concurrency, inputs, testInput)) {
		await scanner.scanInput(input);
		input.processing = false;
		mainWindow.webContents.send('inputChanged', input);
	}
});

ipcMain.on('commit', async (event, input, approved) => {
	try {
		await commit(input, approved);
		await scanner.scanInput(input);
		mainWindow.webContents.send('inputChanged', input);
	} catch (error) {
		console.error(error.stack || error.message || ("Error:" + error));
	}
});

async function commit(input: Input, approved: boolean): Promise<void> {
	const filesToClean = new Set([input.testFile, input.approvedFile, input.rejectedFile, input.diffFile]);
	const baseFile = input.testFile || input.approvedFile || input.rejectedFile;
	filesToClean.delete(baseFile);

	await Promise.all(Array.from(filesToClean).map((file) => {
		if (file) fsp.rm(file);
	}));

	const commitOutput = approved ? config.outputs.approved : config.outputs.rejected;
	const commitFile = util.pathReplace(commitOutput, input as unknown as Props);
	await fsp.rename(baseFile, commitFile);
}

ipcMain.on('tool', async (event, type, toolKey: string, input: Input) => {
	if (type == 'processor') {
		const tool = config.tools.processors[toolKey];

		const tmpDir = await fsp.mkdtemp(path.join(os.tmpdir(), 'visual-regress-'));
		const testFile = path.join(tmpDir, 'test' + (tool.extension || ''));
		const errorFile = path.join(tmpDir, 'error.txt');

		let window = new BrowserWindow({
			width: 800,
			height: 800,
			webPreferences: {
				webSecurity: false,
				plugins: true
			},
		});

		await runner.run(tool.command, {input: input.path, testOutput: testFile}, errorFile);
		try {
			if (!window) return;
			if (tool.extension && tool.extension.match(/\.pdf$/)) {
				let viewerUrl = `file://${app.getAppPath()}/web/pdf.js.asar/web/viewer.html`;
				viewerUrl += '?file=' + encodeURI(testFile);
				viewerUrl += '#zoom=page-fit';
				window.loadURL(viewerUrl);
			} else {
				window.loadURL('file://' + testFile);
			}
		} catch (e) {
			console.error(e);
			window.loadURL('file://' + errorFile);
		}


		window.on('closed', () => {
			window = null;
			fs.rmSync(tmpDir, { recursive: true, force: true });
		});
	} else {
		const tool = config.tools.viewers[toolKey];
		await runner.launch(tool.command, Object.assign({checkedFile: input.approvedFile || input.rejectedFile}, input) as unknown as Props);
	}
});

ipcMain.on('note', async (event, input, note) => {
	const noteFile = util.pathReplace(config.outputs.note, input);
	await fsp.rm(noteFile, { force: true });
	if (note) await fsp.writeFile(noteFile, note);
	await scanner.scanInput(input);
	mainWindow.webContents.send('inputChanged', input);
});
