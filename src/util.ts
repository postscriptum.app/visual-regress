import type {Props} from "./config.js";

function propsReplace(path: string | ((obj: Props) => string), props: Props): string {
	if (typeof path == 'string') {
		let result = path;
		for (const key in props) {
			const prop = props[key];
			if (typeof prop == 'string') result = result.replace(new RegExp('{' + key + '}', 'g'), prop);
		}
		return result;
	} else {
		return path(props);
	}
}

function osPath(file: string): string {
	return process.platform == "win32" ? file.replace(/\//g, '\\') : file;
}

function pathReplace(path: string | ((obj: Props) => string), props: Props): string {
	return osPath(propsReplace(path, props));
}

/**
 * From https://github.com/rxaviers/async-pool
 * @license MIT
 */
async function* asyncPool<R,I>(concurrency: number, iterable: Iterable<I>, iteratorFn: (item: I, iterable?: Iterable<I>) => R): AsyncGenerator<R> {
	const executing = new Set<Promise<[Promise<any>, R]>>();
	async function consume(): Promise<R> {
		const [promise, value] = await Promise.race(executing);
		executing.delete(promise);
		return value;
	}
	for (const item of iterable) {
		// Wrap iteratorFn() in an async fn to ensure we get a promise.
		// Then expose such promise, so it's possible to later reference and
		// remove it from the executing pool.
		const promise: Promise<[Promise<any>, R]> = (async () => await iteratorFn(item, iterable))().then(
			value => [promise, value]
		);
		executing.add(promise);
		if (executing.size >= concurrency) {
			yield await consume();
		}
	}
	while (executing.size) {
		yield await consume();
	}
}

export {
	propsReplace,
	osPath,
	pathReplace,
	asyncPool
};
