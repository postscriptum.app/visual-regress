import * as path from "path";
import type { PDFViewer } from "pdfjs-dist/web/pdf_viewer.mjs";

export type Props = {[key: string]: string | boolean};

type StringFromProps = (input: Props) => string;

export interface ComplexCommmand {
	executable: string,
	args?: (string | StringFromProps)[],
	env?: { [key: string]: string },
	timeout?: number,
	cwd?: string
}

export type Command = string | ComplexCommmand | ((props: Props) => string | ComplexCommmand | Promise<string | ComplexCommmand>);

export interface Tools {
	processors?: { [key: string]: Tool };
	viewers?: { [key: string]: Tool };
}

export interface Tool {
	label: string;
	command: Command;
	extension?: string;
}

export interface Config {
	baseDir?: string;
	inputs: string | string[];
	indexes: string[] | RegExp;
	processor: Command;
	comparator: Command;
	outputs?: {
		test?: string | StringFromProps;
		approved?: string | StringFromProps;
		rejected?: string | StringFromProps;
		diff?: string | StringFromProps;
		error?: string | StringFromProps;
		note?: string | StringFromProps;
	}
	tools?: Tools;
	concurrency?: number;
}

export function createConfig(configFile: string, baseDir?: string): Config {
	const config = require(path.resolve(process.cwd(), configFile)) as Config;
	const configDir = path.resolve(path.dirname(configFile));
	if (!config.baseDir) config.baseDir = baseDir ? path.resolve(baseDir) : configDir;
	else config.baseDir = path.resolve(configDir, config.baseDir);
	return config;
}

declare global {
	interface Window {
		// eslint-disable-next-line @typescript-eslint/naming-convention
		PDFViewerApplication: {
			eventBus: {
				on: (event: string, listener: (event?: any) => void, options?: { once: boolean }) => void
				off: (event: string, listener: (event?: any) => void) => void
			},
			initializedPromise: Promise<void>,
			pdfViewer: PDFViewer
		}
	}
}
