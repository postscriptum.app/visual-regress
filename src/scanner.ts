import * as fs from "fs";
import * as path from "path";

import { glob } from "glob";
import * as flat from "flat";
import * as util from "./util.js";
import type {Config, Props} from "./config.js";

const fsp = fs.promises;

export type State = 'error' | 'approved' | 'rejected' | 'conflict' | 'tested';

export type Output = 'approved' | 'rejected' | 'test' | 'error' | 'diff' | 'note';

const OUTPUTS: Output[] = ['approved', 'rejected', 'test', 'error', 'diff', 'note'];

export interface Input {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	__input: boolean;
	path: string;
	name: string;
	relPath: string;
	extname: string;
	basename: string;
	dirname: string;
	processing: boolean;
	state: State;
	note: string;
	testFile: string;
	approvedFile?: string;
	rejectedFile?: string;
	diffFile?: string;
	errorFile?: string;
	noteFile?: string;
}

export interface InputsTree {
	[key: string]: Input | InputsTree;
}

async function scan(): Promise<Input[]> {
	const {inputs: inputsConfig} = global.config as Config;
	const inputsGlob = Array.isArray(inputsConfig) ? '{' + inputsConfig.join(',') + '}' : inputsConfig;
	const indexes =  global.config.indexes;

	const files = await glob(inputsGlob, {cwd: global.config.baseDir, nodir: true});
	files.sort();
	const inputs: Input[] = [];
	for (let relPath of files) {
		let filename = path.basename(relPath);
		let extname = path.extname(filename);
		const fullpath = path.resolve(global.config.baseDir, relPath);

		if (indexes) {
			let isIndex = false;
			if (Array.isArray(indexes)) isIndex = indexes.includes(filename);
			else isIndex = indexes.test(filename);
			if (isIndex) {
				const parentRelPath = path.dirname(relPath);
				let siblingFound = false;
				for (const file of files) {
					if (file.startsWith(parentRelPath) && file != relPath && !file.substring(parentRelPath.length + 1).includes('/')) {
						siblingFound = true;
						break;
					}
				}

				if (!siblingFound) {
					relPath = parentRelPath;
					filename = path.basename(relPath);
					extname = '';
				}
			}
		}

		const input: Partial<Input> = {
			// eslint-disable-next-line @typescript-eslint/naming-convention
			__input: true,
			name: filename,
			path: fullpath,
			relPath: relPath,
			extname,
			basename: path.basename(filename, extname),
			dirname: path.dirname(path.resolve(global.config.baseDir, relPath))
		};
		await scanInput(input);
		inputs.push(input as Input);
	}
	return inputs;
}

export function inputsTree(inputs: Input[]): InputsTree {
	const filesMap = {};
	for (const input of inputs) {
		filesMap[input.relPath] = input;
	}
	return flat.unflatten(filesMap, {delimiter: path.posix.sep, object: true});
}

async function scanInput(input: Partial<Input>): Promise<void> {
	await Promise.all(OUTPUTS.map((output) => scanOutputs(input, output)));
	if (input.errorFile) input.state = 'error';
	else if (input.diffFile && (input.approvedFile || input.rejectedFile)) input.state = 'conflict';
	else if (input.approvedFile) input.state = 'approved';
	else if (input.rejectedFile) input.state = 'rejected';
	else if (input.testFile) input.state = 'tested';
	else delete input.state;

	if (input.noteFile) {
		input.note = await fsp.readFile(input.noteFile, 'utf8');
	} else {
		delete input.note;
		return null;
	}
}

function scanOutputs(input: Partial<Input>, output: Output): Promise<void> {
	const outputFile = util.pathReplace(global.config.outputs[output], input as unknown as Props);

	return new Promise((resolve) => {
		fs.stat(outputFile, (error, stat) => {
			if (error || !stat.isFile()) {
				if (output + 'File' in input) delete input[output + 'File'];
				resolve();
			} else {
				input[output + 'File'] = outputFile;
				resolve();
			}
		});
	});
}

export {scan, scanInput};

