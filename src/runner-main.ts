import * as os from "os";
import * as path from "path";
import * as commander from "commander";
import * as xml from "xml";
import * as scanner from "./scanner.js";
import * as runner from "./runner.js";
import {createConfig} from "./config.js";

import type {Input, InputsTree} from "./scanner.js";
import {asyncPool} from "./util";

const command = new commander.Command()
	.argument("<configFile>")
	.argument("[baseDir]")
	.option("--format <format>")
	.showHelpAfterError()
	.parse();
const [configFile, baseDir] = command.processedArgs;
const options = command.opts();
const config = global.config = createConfig(configFile, baseDir);

async function writeToConsole(string: string): Promise<void> {
	return new Promise((resolve, reject) => {
		process.stdout.write(string, (error) => {
			if (error) reject(error);
			else resolve();
		});
	});
}

interface Reporter {
	onStart?(inputs: Input[]): Promise<void>;

	onTestSuiteStart?(name: string, inputsTree: InputsTree): Promise<void>;

	onTest?(input: Input, time: number, diff: boolean, error?: any): Promise<void>;

	onTestSuiteEnd?(name: string, inputsTree: InputsTree): Promise<void>;

	onEnd?(inputs: Input[]): Promise<void>;
}

class DefaultReporter implements Reporter {
	diff: Input[] = [];

	async onStart(inputs: Input[]): Promise<void> {
		console.log("States before:");
		this.printStats(inputs);
	}

	async onEnd(inputs: Input[]): Promise<void> {
		await writeToConsole("\n");
		if (this.diff.length) {
			console.log(`${this.diff.length} differences found:`);
			for (const input of this.diff) {
				console.log(` - ${input.relPath}`);
			}
		} else {
			console.log("No difference found.");
		}
		console.log("States after:");
		this.printStats(inputs);
	}

	async onTest(input: Input, time: number, diff: boolean, error?: any): Promise<void> {
		if (error) {
			await writeToConsole("\n");
			console.error(error.stack || error.message || error);
		} else {
			if (diff) this.diff.push(input);
			await writeToConsole(".");
		}

	}

	printStats(inputs: Input[]): void {
		let [errorCount, conflictCount, approvedCount, rejectedCount, testedCount] = [0, 0, 0, 0, 0];
		for (const input of inputs) {
			if (input.errorFile) errorCount++;
			else if (input.diffFile && (input.approvedFile || input.rejectedFile)) conflictCount++;
			else if (input.approvedFile) approvedCount++;
			else if (input.rejectedFile) rejectedCount++;
			else if (input.testFile) testedCount++;
		}
		console.log(` - error: ${errorCount}
	 - conflict: ${conflictCount}
	 - approved: ${approvedCount}
	 - rejected: ${rejectedCount}
	 - tested: ${testedCount}`);
	}
}

class JunitReporter implements Reporter {
	testsuite = [{_attr: {
		name: path.basename(global.config.baseDir),
		timestamp: new Date().toISOString()
	}}] as any[];
	suiteName = [];
	startTime: number;

	async onStart(): Promise<void> {
		this.startTime = Date.now();
	}

	async onTestSuiteStart(name: string): Promise<void> {
		if (name) this.suiteName.push(name);
	}

	async onTest(input: Input, time: number, diff: boolean, error?: any): Promise<void> {
		const testcase = [{
			_attr: {
				classname: this.suiteName.join('/'),
				name: input.basename,
				time: time / 1000
			}
		}] as any[];
		if (error) testcase.push({error: error.stack || error.message || error});
		else if (diff) testcase.push({failure: {_attr: {message: "conflict"}}});

		this.testsuite.push({testcase});
	}

	async onTestSuiteEnd(): Promise<void> {
		this.suiteName.pop();
	}

	async onEnd(): Promise<void> {
		this.testsuite[0]._attr.time = (Date.now() - this.startTime) / 1000;
		process.stdout.write(xml({testsuite: this.testsuite}));
	}
}

const reporter: Reporter = options.format == "junit" ? new JunitReporter() : new DefaultReporter();

(async () => {
	try {
		let hasDiffs = false;
		let hasErrors = false;
		const inputs = await scanner.scan();
		const inputsTree = scanner.inputsTree(inputs);
		if (reporter.onStart) await reporter.onStart(inputs);
		const {suiteHasErrors, suiteHasDiffs} = await testSuite("", inputsTree);
		if (suiteHasErrors) hasErrors = true;
		if (suiteHasDiffs) hasDiffs = true;
		if (reporter.onEnd) await reporter.onEnd(inputs);
		process.exit(hasErrors ? 2 : (hasDiffs ? 1 : 0));
	} catch (error) {
		console.error(error.stack || error.message || error);
	}
})();

function flattenInputsTree(inputsTree: InputsTree): Input[] {
	const suiteInputs: Input[] = [];
	for (const name in inputsTree) {
		const object = inputsTree[name];
		if ('__input' in object) suiteInputs.push(object as Input);
		else suiteInputs.push(...flattenInputsTree(object as InputsTree));
	}
	return suiteInputs;
}

async function testInput(input: Input): Promise<{ hasDiffs: boolean, hasErrors: boolean}> {
	let hasDiffs = false;
	let hasErrors = false;
	let diff, error, time;
	try {
		const startTime = Date.now();
		diff = await runner.test(input);
		time = Date.now() - startTime;
		if (diff) hasDiffs = true;
	} catch (e) {
		error = e;
		hasErrors = true;
	} finally {
		if (reporter.onTest) await reporter.onTest(input, time, diff, error);
		await scanner.scanInput(input);
	}
	return { hasDiffs, hasErrors };
}

async function testSuite(name: string, inputsTree: InputsTree): Promise<{ suiteHasDiffs: boolean, suiteHasErrors: boolean }> {
	if (reporter.onTestSuiteStart) await reporter.onTestSuiteStart(name, inputsTree);
	const suiteInputs: Input[] = flattenInputsTree(inputsTree);

	let suiteHasDiffs = false;
	let suiteHasErrors = false;

	const concurrency = config.concurrency || Math.floor(os.cpus().length / 2);
	for await (const { hasDiffs, hasErrors} of asyncPool(concurrency, suiteInputs, testInput)) {
		if (hasDiffs) suiteHasDiffs = true;
		if (hasErrors) suiteHasErrors = true;
	}
	if (reporter.onTestSuiteEnd) await reporter.onTestSuiteEnd(name, inputsTree);
	return {suiteHasDiffs, suiteHasErrors};
}
