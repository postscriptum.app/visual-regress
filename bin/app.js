#!/usr/bin/env node
const childProcess = require('child_process');
const path = require('path');

const electron = require('electron');
childProcess.spawn(electron, [
	path.resolve(__dirname, '..')
].concat(process.argv.slice(2)), { stdio: 'inherit'});
